var numbers = [12, 1, 3]
// Mengurutkan secara Ascending
numbers.sort(function (value1, value2) { return value1 > value2 } ) ; 
console.log(numbers) // [1, 3, 12]
 
// Mengurutkan secara Descending
numbers.sort(function (value1, value2) { return value1 < value2 } ) ;
console.log(numbers) // [12, 3, 1] 