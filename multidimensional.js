var arrayMulti = [ 
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
// Maka sebagai gambaran, indeks dari array tersebut adalah 
/*
    [
        [(0,0), (0,1), (0,2)],
        [(1,0), (1,1), (1,2)],
        [(2,0), (2,1), (2,2)]
    ] 
*/
console.log(arrayMulti[0][0]) // 1 
console.log(arrayMulti[1][0]) // 4
console.log(arrayMulti[2][1]) // 8